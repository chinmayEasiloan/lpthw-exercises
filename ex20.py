
from sys import argv

script, inputFile = argv

def printAll(f):#print the content  of file
    print(f.read())

def rewind(f):
    f.seek(0)

def printALine(lineCount , f):
    print(lineCount, f.readline()) 

current_file = open(inputFile)
print("First let's print the whole file:\n")

printAll(current_file)#call printAll function
print("Now let's rewind, kind of like a tape.")

rewind(current_file)
print("Let's print three lines:")

current_line = 1
printALine(current_line, current_file)

current_line = current_line + 1
printALine(current_line, current_file)

current_line = current_line + 1
printALine(current_line, current_file)