
from sys import argv
from os.path import exists #new

script, fromFile, toFile = argv

print(f"Copying from {fromFile} to {toFile}")

# we could do these two on one line, how?

inFile = open(fromFile) #inFile has the txt file access
inData = inFile.read()  #read txt file mentioned

print(f"The input file is {len(inData)} bytes long") #get the bytes details

print(f"Ready, hit RETURN to continue, CTRL-C to abort.")
input()

outFile = open(toFile, 'w') #outFile has toFile txt access
outFile.write(inData)

print("Alright, all done.")

outFile.close()
inFile.close()

