
myName = 'Zed A. Shaw'
myAge = 35 # not a lie
myHeight = 74 # inches 
myWeight = 180 #lbs
myEyes = 'Blue'
myTeeth = 'White'
myHair = 'Brown'

print(f"Let's talk about {myName}. ")
print(f"He's {myHeight} inches tall.")
print(f"He's {myWeight} pounds heavy ")
print(f"His teeth are usually {myEyes} eyes and {myHair} hair.")

#this line is tricky, try to get it exactly right

total = myAge + myHeight + myWeight

print(f"If I add {myAge} , {myHeight}, and {myWeight} I get {total}.")


