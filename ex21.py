 
def add(a, b):
    print(f"Adding {a} + {b}")
    return a + b #directly wrote return we did not define while writing method

def sub(a, b):
    print(f"Subtracting {a} - {b}")
    return a - b

def mul(a, b):
    print(f"Multiplying {a} * {b}")
    return a * b

def divide(a, b):
    print(f"Dividing {a} / {b}")
    return a / b    

print("Let's do some math with just functions!")

age = add(30, 5)
height = sub(78, 4)
weight = mul(10 , 6)
iq = divide(100,2)

print(f"Age: {age}, Height: {height}, Weight: {weight}, IQ: {iq}")





