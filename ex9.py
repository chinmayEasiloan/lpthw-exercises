
days = "Mon Tue Wed Thu Fri Sat Sun"
months = "Jan\nFeb\nMar\nMay\nJun\nJul\nAug"

print("Here are the days:", days) #as java we can use a variable and print
print("Here are the months: ", months)

#3 double qoutes 
print("""  
There's something going on here.
with the three double-qoutes.
We'll beable to type as much as we like.
even 4 lines if we want, or 5, or 6.
 """)