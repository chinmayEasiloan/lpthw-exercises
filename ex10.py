

tabbyCat = "\tI'm tabbed in."
persianCat = "I'm split\none a line."
backslashCat = "I'm \\ a \\ cat."

fatCat ="""
I'll do a list:
\t* Cat food
\t* Fishes
\t* Catnip\n\t* Grass
"""

print(tabbyCat)
print(persianCat)
print(backslashCat)
print(fatCat)